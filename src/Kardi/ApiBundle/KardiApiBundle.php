<?php
declare(strict_types=1);

namespace Kardi\ApiBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class KardiApiBundle extends Bundle
{
}
