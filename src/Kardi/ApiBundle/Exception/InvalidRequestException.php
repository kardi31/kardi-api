<?php
declare(strict_types=1);

namespace Kardi\ApiBundle\Exception;

use Exception;

class InvalidRequestException extends Exception
{

}
