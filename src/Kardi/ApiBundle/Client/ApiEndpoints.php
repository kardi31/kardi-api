<?php
declare(strict_types=1);
/**
 * Class ApiEndpoints
 *
 * @author Tomasz Kardas <kardi31@tlen.pl>
 */
namespace Kardi\ApiBundle\Client;

final class ApiEndpoints
{
    public const SEARCH_TEAMS = 'searchteams';
    public const SEARCH_PLAYERS = 'searchplayers';
    public const SEARCH_EVENTS = 'searchevents';
    public const SEARCH_FILENAME = 'searchfilename';
    public const ALL_SPORTS = 'all_sports';
    public const ALL_LEAGUES = 'all_leagues';
    public const ALL_COUNRIES = 'all_countries';
    public const SEARCH_ALL_LEAGUES = 'search_all_leagues';
    public const SEARCH_ALL_SEASONS = 'search_all_seasons';
    public const SEARCH_ALL_TEAMS = 'search_all_teams';
    public const LOOKUP_ALL_TEAMS = 'lookup_all_teams';
    public const LOOKUP_LEAGUE = 'lookupleague';
    public const LOOKUP_TEAM = 'lookupteam';
    public const LOOKUP_PLAYER = 'lookupplayer';
    public const LOOKUP_EVENT = 'lookupevent';
    public const LOOKUP_EVENT_STATS = 'lookupeventstats';
    public const LOOKUP_HONORS = 'lookuphonors';
    public const LOOKUP_FORMER_TEAMS = 'lookupformerteams';
    public const LOOKUP_CONTRACTS = 'lookupcontracts';
    public const EVENT_RESULTS = 'eventresults';
    public const LOOKUP_TABLE = 'lookuptable';
    public const EVENTS_NEXT = 'eventsnext';
    public const EVENTS_NEXT_LEAGUE = 'eventsnextleague';
    public const EVENTS_LAST = 'eventslast';
    public const EVENTS_PAST_LEAGUE = 'eventspastleague';
    public const EVENTS_ROUND = 'eventsround';
}
