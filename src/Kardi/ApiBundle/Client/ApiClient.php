<?php
declare(strict_types=1);
/**
 * Class ApiClient
 *
 * @author Tomasz Kardas <kardi31@tlen.pl>
 */

namespace Kardi\ApiBundle\Client;

use Kardi\ApiBundle\Exception\InvalidRequestException;
use Kardi\ApiBundle\Exception\InvalidResponseException;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class ApiClient implements ApiClientInterface
{
    private const METHOD_GET = 'GET';

    /**
     * @var string
     */
    protected string $apiUrl;

    /**
     * @var string
     */
    protected string $apiKey;

    /**
     * @var LoggerInterface|null
     */
    protected ?LoggerInterface $logger;

    /**
     * @param string $apiUrl
     * @param string $apiKey
     * @param LoggerInterface|null $logger
     */
    public function __construct(string $apiUrl, string $apiKey, ?LoggerInterface $logger = null)
    {
        $this->apiUrl = $apiUrl;
        $this->apiKey = $apiKey;
        $this->logger = $logger ?? new NullLogger();
    }

    /**
     * @param string $endpoint
     * @param string $method
     * @param array $parameters
     * @return ResponseInterface|null
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    protected function doRequest(string $endpoint, string $method, array $parameters = []): ResponseInterface
    {
        $url = sprintf('%s.php?%s', $endpoint, http_build_query($parameters));
        $client = $this->getClient();
        try {
            $result = $client->request($method, $url);
            /**
             * Some API endpoints return blank response when data for request doesn't exist. This is corrected here, as we want to have some indication when the data for request exists or not.
             */
            if (empty($result->getBody()->getContents())) {
                throw new InvalidResponseException('API request was invalid. Response is blank');
            }

            return $result;
        } catch (GuzzleException $e) {
            $this->logger->error($e->getMessage());
            throw new InvalidRequestException(sprintf('Request failed with error message %s', $e->getMessage()));
        }
    }

    /**
     * @return ClientInterface
     */
    protected function getClient(): ClientInterface
    {
        $fullApiUrl = sprintf('%s/%s/', $this->apiUrl, $this->apiKey);

        return new Client([
            'base_uri' => $fullApiUrl,
            'timeout'  => 2.0,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function findTeamByName(string $name): ResponseInterface
    {
        return $this->doRequest(ApiEndpoints::SEARCH_TEAMS, self::METHOD_GET, ['t' => $name]);
    }

    /**
     * @inheritDoc
     */
    public function findTeamByCode(string $code): ResponseInterface
    {
        return $this->doRequest(ApiEndpoints::SEARCH_TEAMS, self::METHOD_GET, ['sname' => $code]);
    }

    /**
     * @inheritDoc
     */
    public function findPlayerByName(string $playerName, ?string $teamName = null): ResponseInterface
    {
        $parameters = [
            'p' => $playerName,
        ];
        if ($teamName) {
            $parameters['t'] = $teamName;
        }
        return $this->doRequest(ApiEndpoints::SEARCH_PLAYERS, self::METHOD_GET, $parameters);
    }

    /**
     * @inheritDoc
     */
    public function findEventByName(string $eventName, ?string $season = null): ResponseInterface
    {
        $parameters = [
            'e' => $eventName,
        ];
        if ($season) {
            $parameters['s'] = $season;
        }
        return $this->doRequest(ApiEndpoints::SEARCH_EVENTS, self::METHOD_GET, $parameters);
    }

    /**
     * @inheritDoc
     */
    public function findEventByFileName(string $fileName): ResponseInterface
    {
        return $this->doRequest(ApiEndpoints::SEARCH_FILENAME, self::METHOD_GET, ['e' => $fileName]);
    }

    /**
     * @inheritDoc
     */
    public function findAllSports(): ResponseInterface
    {
        return $this->doRequest(ApiEndpoints::ALL_SPORTS, self::METHOD_GET);
    }

    /**
     * @inheritDoc
     */
    public function findAllLeagues(): ResponseInterface
    {
        return $this->doRequest(ApiEndpoints::ALL_LEAGUES, self::METHOD_GET);
    }

    /**
     * @inheritDoc
     */
    public function findAllCountries(): ResponseInterface
    {
        return $this->doRequest(ApiEndpoints::ALL_COUNRIES, self::METHOD_GET);
    }

    /**
     * @inheritDoc
     */
    public function findAllLeaguesByCountry(string $country, ?string $sport = null): ResponseInterface
    {
        $parameters = [
            'c' => $country,
        ];
        if ($sport) {
            $parameters['s'] = $sport;
        }
        return $this->doRequest(ApiEndpoints::SEARCH_ALL_LEAGUES, self::METHOD_GET, $parameters);
    }

    /**
     * @inheritDoc
     */
    public function findAllSeasonsByLeague(int $id): ResponseInterface
    {
        return $this->doRequest(ApiEndpoints::SEARCH_ALL_SEASONS, self::METHOD_GET, ['id' => $id]);
    }

    /**
     * @inheritDoc
     */
    public function findAllTeamsByLeagueName(string $leagueName): ResponseInterface
    {
        return $this->doRequest(ApiEndpoints::SEARCH_ALL_TEAMS, self::METHOD_GET, ['l' => $leagueName]);
    }

    /**
     * @inheritDoc
     */
    public function findAllTeamsByCountryAndSport(string $country, string $sport): ResponseInterface
    {
        return $this->doRequest(
            ApiEndpoints::SEARCH_ALL_TEAMS,
            self::METHOD_GET,
            [
                's' => $sport,
                'c' => $country
            ]
        );
    }

    /**
     * @inheritDoc
     */
    public function findAllTeamDetailsByLeagueId(int $id): ResponseInterface
    {
        return $this->doRequest(ApiEndpoints::LOOKUP_ALL_TEAMS, self::METHOD_GET, ['id' => $id]);
    }

    /**
     * @inheritDoc
     */
    public function findLeagueDetailsById(int $id): ResponseInterface
    {
        return $this->doRequest(ApiEndpoints::LOOKUP_LEAGUE, self::METHOD_GET, ['id' => $id]);
    }

    /**
     * @inheritDoc
     */
    public function findTeamDetailsById(int $id): ResponseInterface
    {
        return $this->doRequest(ApiEndpoints::LOOKUP_TEAM, self::METHOD_GET, ['id' => $id]);
    }

    /**
     * @inheritDoc
     */
    public function findPlayerDetailsById(int $id): ResponseInterface
    {
        return $this->doRequest(ApiEndpoints::LOOKUP_PLAYER, self::METHOD_GET, ['id' => $id]);
    }

    /**
     * @inheritDoc
     */
    public function findEventDetailsById(int $id): ResponseInterface
    {
        return $this->doRequest(ApiEndpoints::LOOKUP_EVENT, self::METHOD_GET, ['id' => $id]);
    }

    /**
     * @inheritDoc
     */
    public function findEventStatisticsById(int $id): ResponseInterface
    {
        return $this->doRequest(ApiEndpoints::LOOKUP_EVENT_STATS, self::METHOD_GET, ['id' => $id]);
    }

    /**
     * @inheritDoc
     */
    public function findHonorsByPlayerId(int $id): ResponseInterface
    {
        return $this->doRequest(ApiEndpoints::LOOKUP_HONORS, self::METHOD_GET, ['id' => $id]);
    }

    /**
     * @inheritDoc
     */
    public function findFormerTeamsByPlayerId(int $id): ResponseInterface
    {
        return $this->doRequest(ApiEndpoints::LOOKUP_FORMER_TEAMS, self::METHOD_GET, ['id' => $id]);
    }

    /**
     * @inheritDoc
     */
    public function findContractsByPlayerId(int $id): ResponseInterface
    {
        return $this->doRequest(ApiEndpoints::LOOKUP_CONTRACTS, self::METHOD_GET, ['id' => $id]);
    }

    /**
     * @inheritDoc
     */
    public function findEventResultsByEventId(int $id): ResponseInterface
    {
        return $this->doRequest(ApiEndpoints::EVENT_RESULTS, self::METHOD_GET, ['id' => $id]);
    }

    /**
     * @inheritDoc
     */
    public function findTableByLeagueIdAndSeason(int $leagueId, string $season): ResponseInterface
    {
        return $this->doRequest(
            ApiEndpoints::LOOKUP_TABLE,
            self::METHOD_GET,
            [
                'l' => $leagueId,
                's' => $season,
            ]
        );
    }

    /**
     * @inheritDoc
     */
    public function findNextEventsByTeamId(int $id): ResponseInterface
    {
        return $this->doRequest(ApiEndpoints::EVENTS_NEXT, self::METHOD_GET, ['id' => $id]);
    }

    /**
     * @inheritDoc
     */
    public function findNextEventsByLeagueId(int $id): ResponseInterface
    {
        return $this->doRequest(ApiEndpoints::EVENTS_NEXT_LEAGUE, self::METHOD_GET, ['id' => $id]);
    }

    /**
     * @inheritDoc
     */
    public function findLastEventsByTeamId(int $id): ResponseInterface
    {
        return $this->doRequest(ApiEndpoints::EVENTS_LAST, self::METHOD_GET, ['id' => $id]);
    }

    /**
     * @inheritDoc
     */
    public function findLastEventsByLeagueId(int $id): ResponseInterface
    {
        return $this->doRequest(ApiEndpoints::EVENTS_PAST_LEAGUE, self::METHOD_GET, ['id' => $id]);
    }

    /**
     * @inheritDoc
     */
    public function findEventsByLeagueIdRoundAndSeason(int $leagueId, int $round, string $season): ResponseInterface
    {
        return $this->doRequest(
            ApiEndpoints::EVENTS_ROUND,
            self::METHOD_GET,
            [
                'id' => $leagueId,
                's' => $season,
                'r' => $round,
            ]
        );
    }

    /**
     * @inheritDoc
     */
    public function findEventsByLeagueIdAndSeason(int $leagueId, string $season): ResponseInterface
    {
        return $this->doRequest(
            ApiEndpoints::EVENTS_ROUND,
            self::METHOD_GET,
            [
                'id' => $leagueId,
                's' => $season,
            ]
        );
    }
}
