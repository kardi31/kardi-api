<?php
declare(strict_types=1);
/**
 * Class ApiClientInterface
 *
 * @author Tomasz Kardas <kardi31@tlen.pl>
 */

namespace Kardi\ApiBundle\Client;

use Kardi\ApiBundle\Exception\InvalidRequestException;
use Kardi\ApiBundle\Exception\InvalidResponseException;
use Psr\Http\Message\ResponseInterface;

interface ApiClientInterface
{
    /**
     * @param string $name
     * @return ResponseInterface
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    public function findTeamByName(string $name): ResponseInterface;

    /**
     * @param string $code
     * @return ResponseInterface
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    public function findTeamByCode(string $code): ResponseInterface;

    /**
     * @param string $playerName
     * @param string|null $teamName
     * @return ResponseInterface
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    public function findPlayerByName(string $playerName, ?string $teamName = null): ResponseInterface;

    /**
     * @param string $eventName
     * @param string|null $season
     * @return ResponseInterface
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    public function findEventByName(string $eventName, ?string $season = null): ResponseInterface;

    /**
     * @param string $fileName
     * @return ResponseInterface
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    public function findEventByFileName(string $fileName): ResponseInterface;

    /**
     * @return ResponseInterface
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    public function findAllSports(): ResponseInterface;

    /**
     * @return ResponseInterface
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    public function findAllLeagues(): ResponseInterface;

    /**
     * @return ResponseInterface
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    public function findAllCountries(): ResponseInterface;

    /**
     * @param string $country
     * @param string|null $sport
     * @return ResponseInterface
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    public function findAllLeaguesByCountry(string $country, ?string $sport = null): ResponseInterface;

    /**
     * @param int $id
     * @return ResponseInterface
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    public function findAllSeasonsByLeague(int $id): ResponseInterface;

    /**
     * @param string $leagueName
     * @return ResponseInterface
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    public function findAllTeamsByLeagueName(string $leagueName): ResponseInterface;

    /**
     * @param string $country
     * @param string $sport
     * @return ResponseInterface
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    public function findAllTeamsByCountryAndSport(string $country, string $sport): ResponseInterface;

    /**
     * @param int $id
     * @return ResponseInterface
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    public function findAllTeamDetailsByLeagueId(int $id): ResponseInterface;

    /**
     * @param int $id
     * @return ResponseInterface
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    public function findLeagueDetailsById(int $id): ResponseInterface;

    /**
     * @param int $id
     * @return ResponseInterface
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    public function findTeamDetailsById(int $id): ResponseInterface;

    /**
     * @param int $id
     * @return ResponseInterface
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    public function findPlayerDetailsById(int $id): ResponseInterface;

    /**
     * @param int $id
     * @return ResponseInterface
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    public function findEventDetailsById(int $id): ResponseInterface;

    /**
     * @param int $id
     * @return ResponseInterface
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    public function findEventStatisticsById(int $id): ResponseInterface;

    /**
     * @param int $id
     * @return ResponseInterface
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    public function findHonorsByPlayerId(int $id): ResponseInterface;

    /**
     * @param int $id
     * @return ResponseInterface
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    public function findFormerTeamsByPlayerId(int $id): ResponseInterface;

    /**
     * @param int $id
     * @return ResponseInterface
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    public function findContractsByPlayerId(int $id): ResponseInterface;

    /**
     * @param int $id
     * @return ResponseInterface
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    public function findEventResultsByEventId(int $id): ResponseInterface;

    /**
     * @param int $leagueId
     * @param string $season
     * @return ResponseInterface
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    public function findTableByLeagueIdAndSeason(int $leagueId, string $season): ResponseInterface;

    /**
     * @param int $id
     * @return ResponseInterface
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    public function findNextEventsByTeamId(int $id): ResponseInterface;

    /**
     * @param int $id
     * @return ResponseInterface
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    public function findNextEventsByLeagueId(int $id): ResponseInterface;

    /**
     * @param int $id
     * @return ResponseInterface
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    public function findLastEventsByTeamId(int $id): ResponseInterface;

    /**
     * @param int $id
     * @return ResponseInterface
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    public function findLastEventsByLeagueId(int $id): ResponseInterface;

    /**
     * @param int $leagueId
     * @param int $round
     * @param string $season
     * @return ResponseInterface
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    public function findEventsByLeagueIdRoundAndSeason(int $leagueId, int $round, string $season): ResponseInterface;

    /**
     * @param int $leagueId
     * @param string $season
     * @return ResponseInterface
     * @throws InvalidRequestException
     * @throws InvalidResponseException
     */
    public function findEventsByLeagueIdAndSeason(int $leagueId, string $season): ResponseInterface;
}
