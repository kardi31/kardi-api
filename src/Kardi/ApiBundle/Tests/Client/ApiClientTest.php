<?php
declare(strict_types=1);
/**
 * Class AppClientTest
 *
 * @author Tomasz Kardas <kardi31@tlen.pl>
 */

namespace Kardi\ApiBundle\Tests\Client;

use Kardi\ApiBundle\Client\ApiClient;
use Kardi\ApiBundle\Tests\Sample\ApiClientObject;
use Kardi\ApiBundle\Exception\InvalidRequestException;
use Kardi\ApiBundle\Exception\InvalidResponseException;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

class ApiClientTest extends TestCase
{
    /**
     * @var ApiClientObject
     */
    private $client;

    public function setUp()
    {
        $this->client = new ApiClientObject('https://www.thesportsdb.com/api/v1/json', '1');
        parent::setUp();
    }

    public function testApiClientObjectBuildsCorrectly()
    {
        $this->assertInstanceOf(ApiClient::class, new ApiClient('testurl', 'testkey'));
    }

    public function testFindTeamByNameReturnsValidResponse()
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findTeamByName('Arsenal')
        );
    }

    public function testFindTeamByNameThrowsExceptionForEmptyResponse()
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findTeamByName('random_team');
    }

    public function testFindTeamByNameThrowsExceptionForInvalidRequest()
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findTeamByName('random_team');
    }

    public function testFindTeamByCodeReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findTeamByCode('Ars')
        );
    }

    public function testFindPlayerByNameReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findPlayerByName('Marcus Rashford')
        );
    }

    public function testFindPlayerByNameAndTeamReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findPlayerByName('Marcus Rashford', 'Manchester United')
        );
    }

    public function testFindEventByNameReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findEventByName('Arsenal_vs_Chelsea')
        );
    }

    public function testFindEventByNameAndSeasonReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findEventByName('Arsenal_vs_Chelsea', '2016-2017')
        );
    }

    public function testFindEventByFileNameReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findEventByFileName('English_Premier_League_2015-04-26_Arsenal_vs_Chelsea')
        );
    }

    public function testFindAllSportsReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findAllSports()
        );
    }

    public function testFindAllLeaguesReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findAllLeagues()
        );
    }

    public function testFindAllCountriesReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findAllCountries()
        );
    }

    public function testFindAllLeaguesByCountryReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findAllLeaguesByCountry('England')
        );
    }

    public function testFindAllLeaguesByCountryAndSportReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findAllLeaguesByCountry('England', 'Football')
        );
    }

    public function testFindAllSeasonsByLeagueReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findAllSeasonsByLeague(4328)
        );
    }

    public function testFindAllTeamsByLeagueNameReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findAllTeamsByLeagueName('English Premier League')
        );
    }

    public function testFindAllTeamsByCountryAndSportReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findAllTeamsByCountryAndSport('Spain', 'Football')
        );
    }

    public function testFindAllTeamDetailsByLeagueIdReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findAllTeamDetailsByLeagueId(4328)
        );
    }

    public function testFindLeagueDetailsByIdReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findLeagueDetailsById(4346)
        );
    }

    public function testFindTeamDetailsByIdReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findTeamDetailsById(133604)
        );
    }

    public function testFindPlayerDetailsByIdReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findPlayerDetailsById(34145937)
        );
    }

    public function testFindEventDetailsByIdReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findEventDetailsById(441613)
        );
    }

    public function testFindEventStatisticsByIdReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findEventStatisticsById(1032723)
        );
    }

    public function testFindHonorsByPlayerIdReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findHonorsByPlayerId(34147178)
        );
    }

    public function testFindFormerTeamsByPlayerIdReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findFormerTeamsByPlayerId(34147178)
        );
    }

    public function testFindContractsByPlayerIdReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findContractsByPlayerId(34147178)
        );
    }

    public function testFindEventResultsByEventIdReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findEventResultsByEventId(652890)
        );
    }

    public function testFindTableByLeagueIdAndSeasonReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findTableByLeagueIdAndSeason(4328, '2012-2013')
        );
    }

    public function testFindNextEventsByTeamIdReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findNextEventsByTeamId(133602)
        );
    }

    public function testFindLastEventsByTeamIdReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findLastEventsByTeamId(133602)
        );
    }

    public function testFindNextEventsByLeagueIdReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findNextEventsByLeagueId(4328)
        );
    }

    public function testFindLastEventsByLeagueIdReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findLastEventsByLeagueId(4328)
        );
    }

    public function testFindEventsByLeagueIdRoundAndSeasonReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findEventsByLeagueIdRoundAndSeason(4328, 38, '2018-2019')
        );
    }

    public function testFindEventsByLeagueIdAndSeasonReturnsValidResponse(): void
    {
        $this->client->setClient(ApiClientObject::VALID_RESPONSE);
        $this->assertInstanceOf(
            ResponseInterface::class,
            $this->client->findEventsByLeagueIdAndSeason(4328, '2018-2019')
        );
    }

    public function testFindTeamByCodeThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findTeamByCode('Ars');
    }

    public function testFindPlayerByNameThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findPlayerByName('Marcus Rashford');
    }

    public function testFindPlayerByNameAndTeamThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findPlayerByName('Marcus Rashford', 'Manchester United');
    }

    public function testFindEventByNameThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findEventByName('Arsenal_vs_Chelsea');
    }

    public function testFindEventByNameAndSeasonThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findEventByName('Arsenal_vs_Chelsea', '2016-2017');
    }

    public function testFindEventByFileNameThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findEventByFileName('English_Premier_League_2015-04-26_Arsenal_vs_Chelsea');
    }

    public function testFindAllSportsThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findAllSports();
    }

    public function testFindAllLeaguesThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findAllLeagues();
    }

    public function testFindAllCountriesThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findAllCountries();
    }

    public function testFindAllLeaguesByCountryThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findAllLeaguesByCountry('England');
    }

    public function testFindAllLeaguesByCountryAndSportThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findAllLeaguesByCountry('England', 'Football');
    }

    public function testFindAllSeasonsByLeagueThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findAllSeasonsByLeague(4328);
    }

    public function testFindAllTeamsByLeagueNameThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findAllTeamsByLeagueName('English Premier League');
    }

    public function testFindAllTeamsByCountryAndSportThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findAllTeamsByCountryAndSport('Spain', 'Football');
    }

    public function testFindAllTeamDetailsByLeagueIdThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findAllTeamDetailsByLeagueId(4328);
    }

    public function testFindLeagueDetailsByIdThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findLeagueDetailsById(4346);
    }

    public function testFindTeamDetailsByIdThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findTeamDetailsById(133604);
    }

    public function testFindPlayerDetailsByIdThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findPlayerDetailsById(34145937);
    }

    public function testFindEventDetailsByIdThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findEventDetailsById(441613);
    }

    public function testFindEventStatisticsByIdThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findEventStatisticsById(1032723);
    }

    public function testFindHonorsByPlayerIdThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findHonorsByPlayerId(34147178);
    }

    public function testFindFormerTeamsByPlayerIdThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findFormerTeamsByPlayerId(34147178);
    }

    public function testFindContractsByPlayerIdThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findContractsByPlayerId(34147178);
    }

    public function testFindEventResultsByEventIdThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findEventResultsByEventId(652890);
    }

    public function testFindTableByLeagueIdAndSeasonThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findTableByLeagueIdAndSeason(4328, '2012-2013');
    }

    public function testFindNextEventsByTeamIdThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findNextEventsByTeamId(133602);
    }

    public function testFindLastEventsByTeamIdThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findLastEventsByTeamId(133602);
    }

    public function testFindNextEventsByLeagueIdThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findNextEventsByLeagueId(4328);
    }

    public function testFindLastEventsByLeagueIdThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findLastEventsByLeagueId(4328);
    }

    public function testFindEventsByLeagueIdRoundAndSeasonThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findEventsByLeagueIdRoundAndSeason(4328, 38, '2018-2019');
    }

    public function testFindEventsByLeagueIdAndSeasonThrowsExceptionForInvalidRequest(): void
    {
        $this->client->setClient(ApiClientObject::WITH_INVALID_REQUEST);
        $this->expectException(InvalidRequestException::class);
        $this->client->findEventsByLeagueIdAndSeason(4328, '2018-2019');
    }

    public function testFindTeamByCodeThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findTeamByCode('random value');
    }

    public function testFindPlayerByNameThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findPlayerByName('random value');
    }

    public function testFindPlayerByNameAndTeamThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findPlayerByName('random value', 'random value');
    }

    public function testFindEventByNameThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findEventByName('random value');
    }

    public function testFindEventByNameAndSeasonThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findEventByName('random value', 'random value');
    }

    public function testFindEventByFileNameThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findEventByFileName('random value');
    }

    public function testFindAllSportsThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findAllSports();
    }

    public function testFindAllLeaguesThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findAllLeagues();
    }

    public function testFindAllCountriesThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findAllCountries();
    }

    public function testFindAllLeaguesByCountryThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findAllLeaguesByCountry('random value');
    }

    public function testFindAllLeaguesByCountryAndSportThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findAllLeaguesByCountry('random value', 'random value');
    }

    public function testFindAllSeasonsByLeagueThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findAllSeasonsByLeague(15000000);
    }

    public function testFindAllTeamsByLeagueNameThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findAllTeamsByLeagueName('random value');
    }

    public function testFindAllTeamsByCountryAndSportThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findAllTeamsByCountryAndSport('random value', 'random value');
    }

    public function testFindAllTeamDetailsByLeagueIdThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findAllTeamDetailsByLeagueId(300000);
    }

    public function testFindLeagueDetailsByIdThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findLeagueDetailsById(300000);
    }

    public function testFindTeamDetailsByIdThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findTeamDetailsById(30000000);
    }

    public function testFindPlayerDetailsByIdThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findPlayerDetailsById(300000000);
    }

    public function testFindEventDetailsByIdThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findEventDetailsById(300000000);
    }

    public function testFindEventStatisticsByIdThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findEventStatisticsById(300000000);
    }

    public function testFindHonorsByPlayerIdThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findHonorsByPlayerId(30000000);
    }

    public function testFindFormerTeamsByPlayerIdThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findFormerTeamsByPlayerId(30000000);
    }

    public function testFindContractsByPlayerIdThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findContractsByPlayerId(300000000);
    }

    public function testFindEventResultsByEventIdThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findEventResultsByEventId(300000000);
    }

    public function testFindTableByLeagueIdAndSeasonThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findTableByLeagueIdAndSeason(3000000, 'random value');
    }

    public function testFindNextEventsByTeamIdThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findNextEventsByTeamId(300000000);
    }

    public function testFindLastEventsByTeamIdThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findLastEventsByTeamId(30000000);
    }

    public function testFindNextEventsByLeagueIdThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findNextEventsByLeagueId(30000000);
    }

    public function testFindLastEventsByLeagueIdThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findLastEventsByLeagueId(30000000);
    }

    public function testFindEventsByLeagueIdRoundAndSeasonThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findEventsByLeagueIdRoundAndSeason(3000000, 3000, 'random value');
    }

    public function testFindEventsByLeagueIdAndSeasonThrowsExceptionForEmptyResponse(): void
    {
        $this->client->setClient(ApiClientObject::WITH_EMPTY_RESPONSE);
        $this->expectException(InvalidResponseException::class);
        $this->client->findEventsByLeagueIdAndSeason(3000000, 'random value');
    }
}
