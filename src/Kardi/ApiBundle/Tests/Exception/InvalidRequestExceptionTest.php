<?php
declare(strict_types=1);
/**
 * Class AppClientTest
 *
 * @author Tomasz Kardas <kardi31@tlen.pl>
 */

namespace Kardi\ApiBundle\Tests\Exception;

use Kardi\ApiBundle\Exception\InvalidRequestException;
use Exception;
use PHPUnit\Framework\TestCase;

class InvalidRequestExceptionTest extends TestCase
{
    public function testClassIsValidExceptionClass()
    {
        $this->assertInstanceOf(Exception::class, new InvalidRequestException());
    }
}
