<?php
declare(strict_types=1);
/**
 * Class AppClientTest
 *
 * @author Tomasz Kardas <kardi31@tlen.pl>
 */

namespace Kardi\ApiBundle\Tests\Exception;

use Exception;
use Kardi\ApiBundle\Exception\InvalidResponseException;
use PHPUnit\Framework\TestCase;

class InvalidResponseExceptionTest extends TestCase
{
    public function testClassIsValidExceptionClass()
    {
        $this->assertInstanceOf(Exception::class, new InvalidResponseException());
    }
}
