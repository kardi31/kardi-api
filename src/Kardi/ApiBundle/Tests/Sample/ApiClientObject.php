<?php
declare(strict_types=1);
/**
 * Class ApiClientObject
 *
 * @author Tomasz Kardas <kardi31@tlen.pl>
 */

namespace Kardi\ApiBundle\Tests\Sample;

use Kardi\ApiBundle\Client\ApiClient;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * Class used for testing purposes of API client
 * Simulates Guzzle requests instead of executing real-time connections to API
 */
class ApiClientObject extends ApiClient
{
    public const WITH_EMPTY_RESPONSE = 'with_empty_response';
    public const WITH_INVALID_REQUEST = 'with_invalid_request';
    public const VALID_RESPONSE = 'valid_response';

    /**
     * @var ClientInterface
     */
    private ClientInterface $client;

    /**
     * @return ClientInterface
     */
    protected function getClient(): ClientInterface
    {
        return $this->client;
    }

    /**
     * Sets guzzle response based on passed type
     * @param string $type
     * @return void
     */
    public function setClient(string $type): void
    {
        switch($type)
        {
            case self::WITH_INVALID_REQUEST:
                $response = new Response(500, [], 'Error Communicating with Serve');
                break;
            case self::WITH_EMPTY_RESPONSE:
                $response = new Response(200, [], '');
                break;
            default:
                $response = new Response(200, [], 'Hello, World');
        }

        $mock = new MockHandler([
            $response
        ]);

        $handlerStack = HandlerStack::create($mock);
        $this->client = new Client(['handler' => $handlerStack]);
    }
}
