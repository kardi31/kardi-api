# About

Library for API conntection with https://www.thesportsdb.com/

## Installation

- Add composer repository to your `composer.json`
```
    "repositories": [
        {
            "type": "vcs",
            "url":  "https://kardi31@bitbucket.org/kardi31/kardi-api.git"
        }
    ],
```

- Run `composer require kardi/api "^0.1"`
- Add bundle to your `bundles.php`

```
Kardi\ApiBundle\KardiApiBundle::class => ['all' => true],
```
- Add parameters to your .env file
```
API_URL=https://www.thesportsdb.com/api/v1/json
API_KEY=yourApiKey
```

## Usage

Use `Kardi\ApiBundle\Client\ApiClientInterface` to query the API
